
// DESSSDlg.h : header file
//

#pragma once
#include <vector>

// CDESSSDlg dialog
class CDESSSDlg : public CDialogEx
{
	// Construction
public:
	CDESSSDlg(CWnd* pParent = NULL);	// standard constructor

	// Dialog Data
	enum { IDD = IDD_DESSS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


	// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()


public:
	CString OpenText; //��������� �����
	CString CipherText; //����� ��������
	CString DeCipherText; //����� ����������
	CString Key; //��� ����

	void String_to_BOOL(); //����� � �������� ���
	void String_cipher_to_BOOL();//������������� ����� � �������� ���
	void key_to_BOOL(); //���� � �������� ���
	void create_keys(); //��������� ������

	std::vector <bool> text; 
	std::vector <bool> cipher_text;
	std::vector <bool> key;
	std::vector <bool> T;
	std::vector <bool> decipher_text;

	bool keys_48bit[16][48];
	bool C[17][28];
	bool D[17][28];

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void generateKey();
	afx_msg void OnBnClicked_Gen_Key();
	void GenKey();
	afx_msg void OnBnClickedButton4();
};
