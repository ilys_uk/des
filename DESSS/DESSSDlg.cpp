﻿// DESSSDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DESSS.h"
#include "DESSSDlg.h"
#include "afxdialogex.h"
#include <algorithm>    // std::generate_n
#include <functional> //for std::function
#include <ctime>
#include <cstdlib>
#include <iostream>

typedef std::vector<char> char_array;
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


using namespace std;
// CDESSSDlg dialog



CDESSSDlg::CDESSSDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDESSSDlg::IDD, pParent)
, OpenText(_T("In cryptography, encryption is the process of encoding information. This process converts the original representation of the information, known as plaintext, into an alternative form known as ciphertext. Only authorized parties can decipher a ciphertext back to plaintext and access the original information. Encryption does not itself prevent interference but denies the intelligible content to a would-be interceptor."))
, CipherText(_T(""))
, Key(_T(""))
, DeCipherText(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDESSSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PLAIN_TEXT2, OpenText);
	DDX_Text(pDX, IDC_CIPHER_TEXT, CipherText);
	DDX_Text(pDX, IDC_KEY, Key);
	DDV_MaxChars(pDX, Key, 8); //максимальный размер ключа 8
	DDX_Text(pDX, IDC_DECIPHER_TEXT, DeCipherText);
}

BEGIN_MESSAGE_MAP(CDESSSDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CDESSSDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CDESSSDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &CDESSSDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CDESSSDlg message handlers

BOOL CDESSSDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDESSSDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDESSSDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CDESSSDlg::String_to_BOOL()//перевод текста в двоичную систему
{
	int length = OpenText.GetLength(); //длина начального текста

	if (length % 8 != 0) //Если длина НЕ кратна 8
	{
		for (int i = 0; i < (8 - length % 8); i++) //даполняем начальный текст нулями
			OpenText += '\0';
	}

	int *buffer = new int[OpenText.GetLength()]; //создаём буффер на длину начального текста, содержащий коды символов начального текста
	for (int i = 0; i < OpenText.GetLength(); i++)
	{
		buffer[i] = OpenText.GetAt(i); // Получение кода символа. OpenText.GetLength()*(дополнено нулями) кратно 8
	}

	bool *buffer_bool = new bool[OpenText.GetLength() * 8]; // создание двоичного массива длиной (длина дополненного текста *8)
	text.clear(); //Чистка вектора text
	text.resize(OpenText.GetLength() * 8);// изменение длины вектора 

	int delimoe;//Перевод из 10ой системы координат в 2ую систему
	for (int i = 0; i < OpenText.GetLength(); i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2; //записываем остатки от деления на 2
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			text[i * 8 + j] = buffer_bool[i * 8 + 7 - j]; //остатки записываем в обратном порядке
		}
	}
	delete[] buffer;
	delete[] buffer_bool;


}

void CDESSSDlg::key_to_BOOL() //перевод ключа в двоичную систему
{
	srand((unsigned)time(NULL));
	char symb[36] = "qwertyuiopasdfghjklzxcvbnm123456789";
	char n_name[9] = "qwertyui"; // присвоил n_name какое то значение, ибо по какой то причине она забивалась мусором.
	unsigned int j;
	for (int i = 0; i < 8; i++)
	{
		j = rand() % 36;
		n_name[i] = symb[j];
	}
	Key = n_name;



	int *buffer = new int[8]; // буффер на 8 элемнтов int (8 элементов ключа)
	for (int i = 0; i < 8; i++)
	{
		buffer[i] = Key.GetAt(i); //получаем коды каждого символа ключа
	}

	bool *buffer_bool = new bool[64]; //теперь размер ключа 64 бит (8 байт по 8 бит)
	key.clear();
	key.resize(64);

	int delimoe;
	for (int i = 0; i < 8; i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2; //остатки от деления на 2
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			key[i * 8 + j] = buffer_bool[i * 8 + 7 - j]; //остатки записываем в обратном порядке
		}
	}

	delete[] buffer;
	delete[] buffer_bool;
}


//void CDESSSDlg::setId()
//{
//	id_ = "";
//	id_.reserve(Node::ID_LENGTH); // preallocate storage
//
//	static const std::string str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
//
//	static std::random_device rd;
//	static std::mt19937 generator(rd());
//	static std::uniform_int_distribution<int> dist(0, str.size() - 1);
//
//	for (int i = 0; i < Node::ID_LENGTH; ++i)
//		id_ += str[dist(generator)];
//}

void CDESSSDlg::String_cipher_to_BOOL()//зашифрованный текст
{



	
	int razmer = CipherText.GetLength();

	if (razmer % 8 != 0)
	{
		for (int i = 0; i < (8 - razmer % 8); i++)
			CipherText += '\0';
	}

	int *buffer = new int[CipherText.GetLength()];
	for (int i = 0; i < CipherText.GetLength(); i++)
	{
		buffer[i] = CipherText.GetAt(i);
	}

	bool *buffer_bool = new bool[CipherText.GetLength() * 8];
	cipher_text.clear();
	cipher_text.resize(CipherText.GetLength() * 8);


	int delimoe;
	for (int i = 0; i < CipherText.GetLength(); i++)
	{
		delimoe = buffer[i];
		for (int j = 0; j < 8; j++)
		{
			buffer_bool[i * 8 + j] = delimoe % 2;
			delimoe = delimoe / 2;
		}

		for (int j = 0; j < 8; j++)
		{
			cipher_text[i * 8 + j] = buffer_bool[i * 8 + 7 - j];
		}
	}
	delete[] buffer;
	delete[] buffer_bool;
	

}






void CDESSSDlg::OnBnClickedButton1() //Кнопка ШИФРОВАНИЕ
{
	UpdateData(TRUE);
	CipherText.Empty(); //Чистка переменной второго окна

	//Преобразования Sj для трансформирования 6 - битовых блоков Bj  в 4 - битные B'j
		const int Sj[8][64] = {
		{ 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
		0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
		4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
		15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 },

		{ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
		3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
		0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
		13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 },

		{ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
		13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
		13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
		1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 },

		{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
		13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
		10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
		3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 },

		{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
		14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
		4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
		11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 },

		{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
		10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
		9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 1, 6,
		4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 1 },

		{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
		13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
		1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
		6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 },

		{ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
		1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
		7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
		2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }
	};
	//Перестановка P по которой считаются Значения функции f(Ri-1,k i), применяемой к 32 - битовому блоку B'_1B'_2...B'_8
	const int P[] =
	{
		16, 7, 20, 21, 29, 12, 28, 17,
		1, 15, 23, 26, 5, 18, 31, 10,
		2, 8, 24, 14, 32, 27, 3, 9,
		19, 13, 30, 6, 22, 11, 4, 25
	};
	//Исходный текст (блок 64 бит) преобразуется c помощью начальной перестановки IP
	const int IP[] = { 58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6,
		64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9, 1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7 };
	//Конечная перестановка IP-1 действует на T16 и является обратной к первоначальной перестановке.
	const int IP_1[] = { 40, 8, 48, 16, 56, 24, 64, 32,
		39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30,
		37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28,
		35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26,
		33, 1, 41, 9, 49, 17, 57, 25 };
	//Генерирование ключа ki из начального ключа. Эта перестановка определяется двумя блоками C0 и D0 по 32 бит каждый
	const int IP_for_key[] = { 57, 49, 41, 33, 25, 17, 9, 1,
		58, 50, 42, 34, 26, 18, 10, 2,
		59, 51, 43, 35, 27, 19, 11, 3,
		60, 52, 44, 36, 63, 55, 47, 39,
		31, 23, 15, 7, 62, 54, 46, 38,
		30, 22, 14, 6, 61, 53, 45, 37,
		29, 21, 13, 5, 28, 20, 12, 4 };
	//Каждый из 16-ти ключей ki состоит из 48 бит, выбранных из битов вектора CiDi
	const int roker_key[] = { 14, 17, 11, 24, 1, 5, //таблица 7
		3, 28, 15, 6, 21, 10,
		23, 19, 12, 4, 26, 8,
		16, 7, 27, 20, 13, 2,
		41, 52, 31, 37, 47, 55,
		30, 40, 51, 45, 33, 48,
		44, 49, 39, 56, 34, 53,
		46, 42, 50, 36, 29, 32 };
    //Функция расширения E. Расширяет 32-битовый вектор Ri-1 до 48-битового вектора
	static int expansion[] = { 32, 1, 2, 3, 4, 5, 4, 5, 
		6, 7, 8, 9, 8, 9, 10, 11,
		12, 13, 12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21, 20, 21,
		22, 23, 24, 25, 24, 25, 26, 27,
		28, 29, 28, 29, 30, 31, 32, 1 };

	String_to_BOOL();  //перевод текста исходного в двоичный код


	/*****************ГЕНЕРАЦИЯ КЛЮЧА***************/








	//начальная перестановка
	bool *buffer = new bool[OpenText.GetLength() * 8];
	int number_at_block, number_at_block_with_IP;

	for (int i = 0; i < OpenText.GetLength() / 8; i++) //проходимся по каждому блоку из 8-ми символов
	{
		for (int j = 0; j < 64; j++) //цикл внутри блока по 64 бит
		{
			number_at_block = i * 64 + j; 
			number_at_block_with_IP = i * 64 + IP[j] - 1; //Исходный текст (блок 64 бит) преобразуется c помощью начальной перестановки IP
			
			buffer[number_at_block] = text[number_at_block_with_IP];
		}
	}

	for (int i = 0; i < OpenText.GetLength() * 8; i++)
	{
		text[i] = buffer[i]; // заполненине вектора текстом после начальной перестановки
	}

	delete[] buffer;

	//Работа с ключом
	key_to_BOOL(); //перевод ключа в двоичный код

	//перестановка в ключе 64 бит, теперь значащие разряды в 56ти битах
	bool *buffer_key = new bool[56];

	for (int j = 0; j < 56; j++)
	{
		number_at_block_with_IP = IP_for_key[j] - 1; //Генерирование ключа ki из начального ключа. Эта перестановка определяется двумя блоками 
		number_at_block = j;                         //C0 и D0 по 32 бит каждый

		buffer_key[number_at_block] = key[number_at_block_with_IP];
	}


	for (int i = 0; i < 56; i++)
	{
		key[i] = buffer_key[i]; // заполненине вектора 01010 ключом после начальной перестановки

	}
	delete[] buffer_key;



	//заполняю CiDi. Ci, Di i=1,2,3…получаются из Ci-1, Di-1 одним или двумя левыми циклическими сдвигами согласно таблице 6
	//C0 и D0 получаем из переставленного ключа
	for (int i = 0; i < 28; i++)
	{
		C[0][i] = key[i];
		D[0][i] = key[i + 28];
	}

	//сдвиг влево на один, получаем C1 D1 и C2 D2
	for (int i = 1; i < 3; i++)
	{

		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) C[i][j] = C[i - 1][0];
			C[i][j] = C[i - 1][j + 1];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 1 > 27)) D[i][j] = D[i - 1][0];
			D[i][j] = D[i - 1][j + 1];
		}
	}

	//сдвиг влево на 2, получаем из С2 D2 с 3его по 8й
	for (int i = 3; i < 9; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) C[i][j] = C[i - 1][27 - j - 2];
			C[i][j] = C[i - 1][j + 2];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) D[i][j] = D[i - 1][27 - j - 2];
			D[i][j] = D[i - 1][j + 2];
		}
	}

	//сдвиг влево на 1, получаем из С8 D8 с 9e
	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) C[9][j] = C[8][0];
		C[9][j] = C[8][j + 1];
	}

	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) D[9][j] = D[8][0];
		D[9][j] = D[8][j + 1];
	}

	//сдвиг влево на 2, получаем из С9 D9 с 10х по 15е включительно
	for (int i = 10; i < 16; i++)
	{
		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) C[i][j] = C[i - 1][27 - j - 2];
			C[i][j] = C[i - 1][j + 2];
		}

		for (int j = 0; j < 28; j++)
		{
			if ((j + 2 > 27)) D[i][j] = D[i - 1][27 - j - 2];
			D[i][j] = D[i - 1][j + 2];
		}
	}

	//сдвиг влево на 1, получаем из С15 D15  16е
	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) C[16][j] = C[15][0];
		C[16][j] = C[15][j + 1];
	}

	for (int j = 0; j < 28; j++)
	{
		if ((j + 1 > 27)) D[16][j] = D[15][0];
		D[16][j] = D[15][j + 1];
	}

	//Ключ ki представляет собой объединение CiDi
	for (int i = 0; i < 16; i++)
	{
		bool *buf = new bool[56];
		for (int k = 0; k < 56; k++)
		{
			if (k < 28) buf[k] = C[i + 1][k];
			else buf[k] = D[i + 1][k - 28];
		}

		for (int k = 0; k < 48; k++)//из 56 делаем 48 ключ
		{
			number_at_block_with_IP = roker_key[k] - 1; //Ключ ki, i = 1, …16 состоит из 48 бит, выбранных из битов вектора CiDi(64 бит) согласно таблице 7
			number_at_block = k;

			keys_48bit[i][number_at_block] = buf[number_at_block_with_IP];
		}
		delete[]buf;
	}



	 //Циклы шифрования
	T.clear();
	T.resize(OpenText.GetLength() * 8);

	for (int i = 0; i < text.size() / 64; i++) //прохожусь по блокам по 64 бита
	{		
		bool *L = new bool[32]; //L- левая
		bool *R = new bool[32]; // R - правая часть

//Разбиваю текст после начальной перестановки на две части L0, R0, где L0, R0 — соответственно 32 старших битов и 32 младших битов блока
		for (int k = 0; k < 32; k++)
		{
			L[k] = text[i * 64 + k];
			R[k] = text[i * 64 + 32 + k];
		}


		for (int ii = 1; ii < 17; ii++)///непосредственно сам цикл из 16 итераций
		{
			bool *E = new bool[48]; 
			for (int k = 0; k < 48; k++)              //в цикле расширяем R до E(R[i-1])
			{
				number_at_block_with_IP = expansion[k] - 1;
				number_at_block = k;
				E[number_at_block] = R[number_at_block_with_IP]; //Функция расширения E
			}

			for (int k = 0; k < 48; k++)           //Полученный после перестановки блок E(Ri-1) складывается по модулю 2 с ключами  ki 
			{
				E[k] = E[k] ^ keys_48bit[ii - 1][k]; 
			}
			//и затем представляется в виде восьми последовательных блоков B1, B2, ...B8
			bool **B = new bool *[8];   //динамически выделяю память под думерный массив B1...B8 (8 блоков по 6 бит в каждом)
			for (int k = 0; k < 8; k++)
				B[k] = new bool[6]; // для 8 блоков выделяем память на 6 бит в каждом из этих 8 блоков

			for (int k = 0; k < 8; k++)   //заполню блоки В1..В8
			{
				for (int j = 0; j < 6; j++)
				{
					B[k][j] = E[k * 6 + j]; // заполняю блоки заксоренным массивом ключа и текста
				}
			}

			delete[]E;

			bool **BB = new bool*[8];   //динамически выделяю память под думерный массив B1...B8 из блоков делаем блоки не по 6 бит, а по 4 бита, блоков так и остается 8
			for (int k = 0; k < 8; k++) 
				BB[k] = new bool[4];


			for (int k = 0; k < 8; k++)   //переделываю Вi 6ти битные в 4х битные
			{
				int a, b, c;
				//Первый и последний разряды Bi являются двоичной записью числа а, средние 4 разряда представляют число b. 
				//Строки таблицы S3 нумеруются от 0 до 3, столбцы таблицы S3 нумеруются от 0 до 15. 
				a = B[k][5] * 1 + B[k][0] * 2;// строка в матрице
				b = B[k][1] * 2 * 2 * 2 + B[k][2] * 2 * 2 + B[k][3] * 2 + B[k][4] * 1;// столбец в матрице
				c = Sj[k][a * 16 + b]; // Пара чисел(а, b) определяет число, находящееся в пересечении строки а и столбца b.Двоичное представление этого числа дает B'i.
				
				for (int j = 0; j < 4; j++) //двоичное представление получившего на пересечении числа
				{
					BB[k][3 - j] = c % 2;
					c = c / 2;
				}
			}

			for (int k = 0; k < 8; k++) delete[] B[k];
			delete[] B;

			bool *f = new bool[32];
			bool *buf = new bool[32];
			for (int k = 0; k < 8; k++)
			{
				for (int j = 0; j < 4; j++)
				{
					buf[k * 4 + j] = BB[k][j]; // получившиеся 8 блоков по 4 бита в каждом записываем в 32-битный массив
				}
			}

			for (int k = 0; k < 8; k++) 
				delete[] BB[k];
			delete[] BB;

			for (int k = 0; k < 32; k++)             //перестановка Р полученного 32-битного массива  для создания функции f(R[i-1],k[i])
			{
				number_at_block_with_IP = P[k] - 1;
				number_at_block = k;
				f[number_at_block] = buf[number_at_block_with_IP];
			}
			delete[] buf;


			bool *L_past = new bool[32];
			for (int k = 0; k < 32; k++)
			{
				L_past[k] = L[k];// левую часть в новый массив
				L[k] = R[k];// Левая половина Li равна правой половине предыдущего вектора R(i - 1)
			}

			for (int k = 0; k < 32; k++)
			{
				R[k] = L_past[k] ^ f[k]; //А правая половина Ri — это битовое сложение L(i - 1) и f(R(i - 1),k(i)) по модулю 2.
			}
			delete[] f;
			delete[]L_past;
		}




		bool *T_buf = new bool[64];
		for (int k = 0; k < 64; k++)
		{
			if (k < 32) T_buf[k] = L[k]; //создаем новый массив для полученных L и R
			else T_buf[k] = R[k - 32];
		}

		for (int k = 0; k < 64; k++)
		{
			number_at_block_with_IP = IP_1[k] - 1;
			number_at_block = k;
			T[i * 64 + number_at_block] = T_buf[number_at_block_with_IP]; //Конечная перестановка полученного массива
		}

		delete[] T_buf;
		delete[]L;
		delete[] R;


	}

	int *cipher = new int[T.size() / 8];   //целочисленный массив зашифрованного текста
	for (int k = 0; k < T.size() / 8; k++)
	{
		//Перевод из двоичной в десятичную систему
		cipher[k] = (int)(T[7 + 8 * k] * 1 + T[6 + 8 * k] * 2 + T[5 + 8 * k] * 2 * 2 + T[4 + 8 * k] * 2 * 2 * 2 + T[3 + 8 * k] * 2 * 2 * 2 * 2 + T[2 + 8 * k] * 2 * 2 * 2 * 2 * 2 + T[1 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 + T[0 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 * 2);
	}

	for (int k = 0; k < T.size() / 8; k++)
	{
		
		CipherText.AppendChar(cipher[k]); //Шифр
	}
	delete[] cipher;
	UpdateData(FALSE);

}


void CDESSSDlg::OnBnClickedButton2() //Расшифровка
{
	//UpdateData(TRUE); 
	DeCipherText.Empty();
	
	

	const int Sj[8][64] = {
		{ 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
		0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
		4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
		15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 },

		{ 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
		3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
		0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
		13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 },

		{ 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
		13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
		13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
		1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 },

		{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
		13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
		10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
		3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 },

		{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
		14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
		4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
		11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 },

		{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
		10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
		9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 1, 6,
		4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 1 },

		{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
		13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
		1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
		6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 },

		{ 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
		1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
		7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
		2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 }
	};

	const int P[] =
	{
		16, 7, 20, 21, 29, 12, 28, 17,
		1, 15, 23, 26, 5, 18, 31, 10,
		2, 8, 24, 14, 32, 27, 3, 9,
		19, 13, 30, 6, 22, 11, 4, 25
	};

	const int IP[] = { 58, 50, 42, 34, 26, 18, 10, 2,
		60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6,
		64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9, 1,
		59, 51, 43, 35, 27, 19, 11, 3,
		61, 53, 45, 37, 29, 21, 13, 5,
		63, 55, 47, 39, 31, 23, 15, 7 };

	const int IP_1[] = { 40, 8, 48, 16, 56, 24, 64, 32,
		39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30,
		37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28,
		35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26,
		33, 1, 41, 9, 49, 17, 57, 25 };


	const int IP_for_key[] = { 57, 49, 41, 33, 25, 17, 9, 1,
		58, 50, 42, 34, 26, 18, 10, 2,
		59, 51, 43, 35, 27, 19, 11, 3,
		60, 52, 44, 36, 63, 55, 47, 39,
		31, 23, 15, 7, 62, 54, 46, 38,
		30, 22, 14, 6, 61, 53, 45, 37,
		29, 21, 13, 5, 28, 20, 12, 4 };

	const int roker_key[] = { 14, 17, 11, 24, 1, 5,
		3, 28, 15, 6, 21, 10,
		23, 19, 12, 4, 26, 8,
		16, 7, 27, 20, 13, 2,
		41, 52, 31, 37, 47, 55,
		30, 40, 51, 45, 33, 48,
		44, 49, 39, 56, 34, 53,
		46, 42, 50, 36, 29, 32 };

	static int expansion[] = { 32, 1, 2, 3, 4, 5, 4, 5,
		6, 7, 8, 9, 8, 9, 10, 11,
		12, 13, 12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21, 20, 21,
		22, 23, 24, 25, 24, 25, 26, 27,
		28, 29, 28, 29, 30, 31, 32, 1 };
	
	String_cipher_to_BOOL();
	decipher_text.clear();
	decipher_text.resize(CipherText.GetLength() * 8);
	int number_at_block_with_IP, number_at_block;


	 

	for (int i = 0; i < cipher_text.size() / 64; i++) //прохожусь по блокам по 64 бита
	{
		bool *L = new bool[32];
		bool *R = new bool[32];
		bool *T_buf = new bool[64];

		for (int k = 0; k < 64; k++)
		{
			number_at_block_with_IP = IP_1[k] - 1;
			number_at_block = k + i * 64;
			T_buf[number_at_block_with_IP] = cipher_text[number_at_block];
		}

		for (int k = 0; k < 64; k++) //делим блок на левую и правую часть
		{
			if (k < 32) L[k] = T_buf[k];
			else R[k - 32] = T_buf[k];
		}
		delete[] T_buf;


		for (int ii = 16; ii>0; ii--)///непосредственно сам цикл из 16 итераций
		{
			bool *E = new bool[48];
			for (int k = 0; k < 48; k++)              //в цикле расширяем L до E(L[i-1])
			{
				number_at_block_with_IP = expansion[k] - 1;
				number_at_block = k;
				E[number_at_block] = L[number_at_block_with_IP];
			}

			for (int k = 0; k < 48; k++)           //побитовое исключающее ИЛИ
			{
				E[k] = E[k] ^ keys_48bit[ii - 1][k];
			}

			bool **B = new bool *[8];   //динамически выделяю память под думерный массив B1...B8
			for (int k = 0; k < 8; k++) B[k] = new bool[6];

			for (int k = 0; k < 8; k++)   //заполню блоки В1..В8
			{
				for (int j = 0; j < 6; j++)
				{
					B[k][j] = E[k * 6 + j];
				}
			}

			delete[]E;

			bool **BB = new bool*[8];   //динамически выделяю память под думерный массив B1...B8
			for (int k = 0; k < 8; k++) BB[k] = new bool[4];


			for (int k = 0; k < 8; k++)   //переделываю В 6ти битные в 4х битные
			{
				int a, b, c;
				a = B[k][5] * 1 + B[k][0] * 2;
				b = B[k][1] * 2 * 2 * 2 + B[k][2] * 2 * 2 + B[k][3] * 2 + B[k][4] * 1;
				c = Sj[k][a * 16 + b];

				for (int j = 0; j < 4; j++)
				{
					BB[k][3 - j] = c % 2;
					c = c / 2;
				}
			}

			for (int k = 0; k < 8; k++) delete[] B[k];
			delete[] B;

			bool *f = new bool[32];
			bool *buf = new bool[32];
			for (int k = 0; k < 8; k++)
			{
				for (int j = 0; j < 4; j++)
				{
					buf[k * 4 + j] = BB[k][j];
				}
			}

			for (int k = 0; k < 8; k++) delete[] BB[k];
			delete[] BB;

			for (int k = 0; k < 32; k++)              //перестановка Р для создания функции f от L[i] and k[i]
			{
				number_at_block_with_IP = P[k] - 1;
				number_at_block = k;
				f[number_at_block] = buf[number_at_block_with_IP];
			}
			delete[] buf;


			bool *R_past = new bool[32];
			for (int k = 0; k < 32; k++)
			{
				R_past[k] = R[k];
				R[k] = L[k];
			}

			for (int k = 0; k < 32; k++)
			{
				L[k] = R_past[k] ^ f[k];
			}
			delete[] f;
			delete[]R_past;
		}

		for (int k = 0; k < 64; k++) // объединяем левую и правую часть
		{
			if (k < 32) decipher_text[i * 64 + k] = L[k];
			else decipher_text[i * 64 + k] = R[k - 32];
		}

		delete[] R;
		delete[]L;

	}


	//перестановка в блоках(которая первая была)
	bool *buffer = new bool[CipherText.GetLength() * 8];
	for (int i = 0; i < CipherText.GetLength() / 8; i++)
	{
		for (int j = 0; j < 64; j++)
		{
			number_at_block_with_IP = i * 64 + IP[j] - 1;
			number_at_block = i * 64 + j;
			buffer[number_at_block_with_IP] = decipher_text[number_at_block];
		}
	}

	for (int i = 0; i < OpenText.GetLength() * 8; i++)
	{
		decipher_text[i] = buffer[i];
	}
	delete[]buffer;

	int *decipher = new int[decipher_text.size() / 8];   //целочисленный массив зашифрованного текста
	for (int k = 0; k < T.size() / 8; k++)
	{
		decipher[k] = (int)(decipher_text[7 + 8 * k] * 1 + decipher_text[6 + 8 * k] * 2 + decipher_text[5 + 8 * k] * 2 * 2 + decipher_text[4 + 8 * k] * 2 * 2 * 2 + decipher_text[3 + 8 * k] * 2 * 2 * 2 * 2 + decipher_text[2 + 8 * k] * 2 * 2 * 2 * 2 * 2 + decipher_text[1 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 + decipher_text[0 + 8 * k] * 2 * 2 * 2 * 2 * 2 * 2 * 2);
	}

	for (int k = 0; k < decipher_text.size() / 8; k++)
	{
		DeCipherText.AppendChar(decipher[k]);
	}
	delete[] decipher;
	UpdateData(FALSE);
}

void CDESSSDlg::generateKey()
{
	

}

	char_array charset()
	{
		//Change this to suit
		return char_array(
		{ '0', '1', '2', '3', '4',
		'5', '6', '7', '8', '9',
		'A', 'B', 'C', 'D', 'E', 'F',
		'G', 'H', 'I', 'J', 'K',
		'L', 'M', 'N', 'O', 'P',
		'Q', 'R', 'S', 'T', 'U',
		'V', 'W', 'X', 'Y', 'Z',
		'a', 'b', 'c', 'd', 'e', 'f',
		'g', 'h', 'i', 'j', 'k',
		'l', 'm', 'n', 'o', 'p',
		'q', 'r', 's', 't', 'u',
		'v', 'w', 'x', 'y', 'z'
		});
	};

	// given a function that generates a random character,
	// return a string of the requested length
	std::string random_string(size_t length, std::function<char(void)> rand_char)
	{
		std::string str(length, 0);
		generate_n(str.begin(), length, rand_char);
		return str;
	}
 
	
	void CDESSSDlg::GenKey()
	{




		srand((unsigned)time(NULL));
		char symb[36] = "qwertyuiopasdfghjklzxcvbnm123456798";
		char n_name[10] = "asdfghjkl"; // присвоил n_name какое то значение, ибо по какой то причине она забивалась мусором.

		unsigned int j;
		for (int i = 0; i < 7; i++)
		{
			j = rand() % 36;
			n_name[i] = symb[j];
		}
		Key = n_name;

	}
	


	void CDESSSDlg::OnBnClickedButton4()
	{
		// TODO: добавьте свой код обработчика уведомлений
	}
